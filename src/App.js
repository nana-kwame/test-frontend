import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.scss";
import petlabapi from "./api/petlabapi";
import {
  Card,
  Button,
  SkeletonLoader,
  Input,
  Toggle,
} from "./components";
import pet from "./assets/pet.png";

const tags = ["Dog", "Cat", "Chews", "Formula", "Shampoo"];
class App extends Component {
  state = {
    loading: false,
    products: [],
    page: 1,
    limit: 8,
    tag: "",
    price: 0,
    subscription: false,
    query: "",
    filter: false,
  };
  componentDidMount() {
    this.fetchProduct();

    // //** Default query */
    //  let { query, subscription } = this.state;
    //  query = `?subscription=${subscription}`;
  }

  //** Fetch Product with pagination */
  fetchProduct = () => {
    const { page, limit } = this.state;

    //** Fetch the data from the mock server */
    this.setState({ loading: !this.state.loading });
    petlabapi
      .get(`/products?_page=${page}&_limit=${limit}`)
      .then((products_res) => {
        //** Object destructring from product response */
        const { data } = products_res;
        this.setState({ loading: !this.state.loading, products: data });

        console.log("[PRODUCTS]:: ", products_res);
      })
      .catch((err) => {
        this.setState({ loading: !this.state.loading });
        console.log("[ERROR]:: ", err);
      });
  };

  //** FILTER  */
  filterProducts = () => {
    let { query, tag, subscription, price } = this.state;

    //** CHECK IF SUBSCRIPTION HAS BEEN TOGGLED */
    if (subscription) {
      query.length === 0
        ? (query += `?subscription=${subscription}`)
        : (query += `&subscription=${subscription}`);

      this.setState({ query });
    }

    if (tag.trim() !== "") {
      query.length === 0
        ? (query += `?tags_like=${tag}`)
        : (query += `&tags_like=${tag}`);

      if (tag === "Dog") {
        query += `&_limit=5`;
      }

      this.setState({ query });
    }

    if (price !== 0 || price == undefined) {
      query.length === 0
        ? (query += `?price_lte=${price}`)
        : (query += `&price_lte=${price}`);

      this.setState({ query });
    }

    //** CHECK IF QUERY HAS BEEN PROVIDED */

    if (query.length !== 0) {
      //** Toggle loader and set query */
      this.setState({ loading: !this.state.loading });
      petlabapi
        .get(`/products${query}`)
        .then((products_res) => {
          //** Object destructring from product response */
          const { data } = products_res;
          this.setState({
            loading: !this.state.loading,
            products: data,
            filter: true,
          });

          console.log("[PRODUCTS]:: ", products_res);
        })
        .catch((err) => {
          this.setState({ loading: !this.state.loading, filter: false });
          console.log("[ERROR]:: ", err);
        });
    }

    //RESET QUERY AT THE END
    query = "";
    this.setState({ query });
  };

  //** CLEAR PRODUCT FILTER */
  clearFilter = () => {
    let { subscription, tag, price, filter } = this.state;

    subscription = false;
    tag = "";
    price = 0;
    filter = false;

    this.setState({ subscription, tag, price, filter });
    this.fetchProduct();
  };

  //** RENDER SIDEBAR */
  renderSideBar = () => {
    const { subscription, query } = this.state;
    return (
      <div className="app__sidebar">
        <img src={logo} className="app__logo" alt="logo" />

        <div className="app__sidebar--filters">
          {/* TAGS */}
          <h5 className="app__sidebar--filters__title">Tags</h5>
          <div className="app__sidebar--filters__row">
            {tags.map((tag, i) => (
              <div
                onClick={() => this.setState({ tag })}
                key={`tag-${tag}-${i}`}
                className={
                  this.state.tag === tag
                    ? "app__sidebar--tag__active"
                    : "app__sidebar--tag"
                }
              >
                <p className="app__sidebar--tag__text">{tag}</p>
              </div>
            ))}
          </div>

          {/* PRICE */}
          {/* <h5 className="app__sidebar--filters__title">Price</h5> */}

          <Input
            placeholder="Price"
            type="number"
            onchange={(evt) => this.setState({ price: evt.target.value })}
            value={this.state.price}
          />

          {/* SUBSCRIPTION */}
          <label className="app__sidebar--filters__sub">
            <span>Toggle Subscription</span>
            <Toggle
              isToggled={subscription}
              onToggle={() =>
                this.setState({ subscription: !this.state.subscription })
              }
            />
          </label>
        </div>

        <div className="app__sidebar--btnRow">
          <Button onclick={() => this.filterProducts()} text="Filter" />
          <Button onclick={() => this.clearFilter()} text="Clear" />
        </div>
      </div>
    );
  };

  //** Next page */
  getNextPage = () => {
    let { page } = this.state;
    page++;
    this.setState({ page }, () => {
      this.fetchProduct();
    });
  };

  //** Previous page */
  getPrevPage = () => {
    let { page } = this.state;
    page--;
    this.setState({ page }, () => {
      this.fetchProduct();
    });
  };

  renderPaginateBar = () => {
    const { page, filter } = this.state;
    return (
      <div className="app__main--paginate">
        <Button
          onclick={() => this.getPrevPage()}
          disable={page === 1 || filter}
          text="Prev"
        ></Button>
        <h6 className="app__main--paginate__text">{page}</h6>
        <Button
          onclick={() => this.getNextPage()}
          disable={page === 2 || filter}
          text="Next"
        ></Button>
      </div>
    );
  };

  renderContent = () => {
    const { loading, products } = this.state;
    return (
      <div className="app__main">
        <h2 className="app__main--title">
          Welcome to PetLab Product Collection
        </h2>

        <Card>
          <table className="app__main--table">
            <thead className="app__main--table__head">
              <tr>
                <th scope="col" className="app__main--table__title">
                  Image
                </th>
                <th scope="col" className="app__main--table__title">
                  Name
                </th>
                <th scope="col" className="app__main--table__title">
                  Price
                </th>
                <th scope="col" className="app__main--table__title">
                  Discount
                </th>
                <th scope="col" className="app__main--table__title">
                  Option Value
                </th>
                <th scope="col" className="app__main--table__title">
                  Tags
                </th>
                {/* <th scope="col" className="app__main--table__title">
                  Image
                </th> */}
              </tr>
            </thead>

            <tbody>
              {loading ? (
                <React.Fragment>
                  <tr>
                    <SkeletonLoader count={6} />
                  </tr>
                  <tr>
                    <SkeletonLoader count={6} />
                  </tr>
                  <tr>
                    <SkeletonLoader count={6} />
                  </tr>
                  <tr>
                    <SkeletonLoader count={6} />
                  </tr>
                  <tr>
                    <SkeletonLoader count={6} />
                  </tr>
                  <tr>
                    <SkeletonLoader count={6} />
                  </tr>
                  <tr>
                    <SkeletonLoader count={6} />
                  </tr>
                  <tr>
                    <SkeletonLoader count={6} />
                  </tr>
                </React.Fragment>
              ) : (
                products.length !== 0 &&
                products.map((product, i) => (
                  <tr
                    key={`product-${product.id}`}
                    className="app__main--table--product"
                  >
                    <td>
                      <img
                        className="app__main--table--product__img"
                        src={product.image_src}
                        onError={(e) => {
                          e.target.onerror = null;
                          e.target.src = pet;
                        }}
                      />
                    </td>

                    <td>{product.title}</td>
                    <td>{product.price}</td>
                    <td>
                      {product.subscription_discount === ""
                        ? 0
                        : product.subscription_discount}
                    </td>
                    <td>{product.option_value}</td>
                    <td className="app__main--table__row">
                      {product.tags.map((tag, i) => (
                        <div
                          key={`product-tag-${product.id}-${tag}`}
                          className="app__main--table__tag"
                        >
                          {tag}
                        </div>
                      ))}
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>

          {products.length === 0 && (
            <div className="app__main--table__empty">
              <img src={pet} className="app__main--table__empty--img" />
              <h4>No Matches Found</h4>
            </div>
          )}
          {this.renderPaginateBar()}
        </Card>
      </div>
    );
  };

  render() {
    return (
      <div className="app">
        {/* SIDEBAR */}
        {this.renderSideBar()}
        {/* MAIN-CONTENT */}
        {this.renderContent()}
      </div>
    );
  }
}

export default App;

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
