import axios from "axios";

const url = "http://localhost:3010"; // --> UAT url

export default axios.create({
  baseURL: url,
});
