import React from "react";
import Skeleton from "react-skeleton-loader";

const SkeletonLoader = ({ count }) => {
  let loaders = [];
  for (let i = 0; i < count; i++) {
    loaders.push(
      <td
        key={`skeleton-loader-${count}-${Math.random().toString(36).substr(2)}`}
      >
        <Skeleton />
      </td>
    );
  }

  return <>{loaders}</>;
};

export default SkeletonLoader;
