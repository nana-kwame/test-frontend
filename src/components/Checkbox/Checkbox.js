import "react-input-checkbox/lib/react-input-checkbox.min.css";

import { Checkbox } from "react-input-checkbox";

const Cbox = ({ value, onChange, disabled, name }) => {
  return (
    <Checkbox value={value} onChange={onChange} theme="bootstrap-checkbox">
      {name}
    </Checkbox>
  );
};

export default Cbox;
