import React from "react";
import "./Button.scss";
const Button = ({ secondary, text, onclick, style, disable }) => {
  const dis = disable && "button__disabled";
  return (
    <button
      onClick={onclick}
      className={
        secondary ? `button secondary ${dis}` : `button primary ${dis}`
      }
      style={style}
      disabled={disable}
    >
      {text}
    </button>
  );
};

export default Button;

Button.defaultProps = {
  disable: false,
};
