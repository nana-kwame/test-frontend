SOLUTION
========

Estimation
----------
Estimated: 15 hours

Spent: 8 hours


Solution
--------
<!-- COMPONENTS AND STYLING -->
I took some creative decisions on the styling and placement of the components on the page & font-styles.

I also decided to take some time to create css variables, if this project was to grow in scale having a consistent theming pattern would make onboarding new team members simpler and faster as they would have a single source of truth for component & page styling as dictated from designs (and shared components)

<!-- APP LOGIC -->
Unfortunately the json-server was unable to provide the first, prev, next and last links as indicated in the documentation, or I was unable to configure the library properly to get that feature set (further instruction would be welcomed and encouraged if that is the case here). My work around here was to disable the next button on pagination after page 2 has been fetched and disabled completely during user filters so as not to break the user experience.

<!-- RESPONSIVENESS -->
This application is not fully responsive, although built with css grid & flexbox there are screen sizes that it is not optimized for and is recommended it be viewed in desktop view.

<!-- NOTES FOR THE FUTURE -->
Certain elements on the page can be pulled into components of their own, eg: sidebar and the table, but I believed that would make the logic a bit more complex and also cut down on the delivery time

There are no empty states or error messages provided



<!-- TEST CASES -->
FEATURE: Display Products
SCENARIO: Populate table on application load
GIVEN user is on the default page (product collection page)
AND the application has finished loading
THEN the table should be populated with a default of 8 items
AND pagination at the bottom


FEATURE: Browse Products
SCENARIO: Browsing through paginated table with data
GIVEN the pagination buttons (Next/Prev) is fully visible
AND the user clicks on them they will be navigated to the next or previous pages respectively

FEATURE: Filter Products
SCENARIO: Filtering table based on queries selected
GIVEN the user is on the sidebar 
WHEN the user either clicks on a tag, toggles the subscriptions or provides a price
AND clicks on filter
THEN the products will be filtered by the query provided
AND the table will display the products that match the provided query

FEATURE: Clear Filter
SCENARIO: Resetting the filters to the default 
GIVEN the user is on the sidebar 
AND has completed their filter
WHEN the user clicks on the clear button
THEN the table would be reset to display the inital default products
BUT with pagination disabled




